CREATE TABLE `Factura`
(
	`NroFactura` int NOT NULL,
	`Periodo` int NOT NULL,
	`ImporteTotal` int NOT NULL,
	`ImporteIVA` int NOT NULL,
   	`ImporteNeto` int NOT NULL, 
	`Estado` char(1) NOT NULL,
	PRIMARY KEY (`NroFactura`),
	CHECK (`Estado` = 'A' OR `Estado` = 'I')
);

CREATE TABLE `FacturaServicio`
(	
	`NroServicio` int NOT NULL,
	`NroFactura` int NOT NULL,
	`ImporteTotal` int NOT NULL,
	`ImporteIVA` int NOT NULL,
	`ImporteNeto` int NOT NULL,    
   	`PorcentajeIVA` float NOT NULL,   
	PRIMARY KEY (`NroServicio`),
	FOREIGN KEY (`NroFactura`) REFERENCES `Factura`(`NroFactura`)
);