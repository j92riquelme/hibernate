package entidades;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "factura", schema = "orm", catalog = "")
public class FacturaEntity {

    @Id
    @Column(name = "NroFactura", nullable = false)
    private int nroFactura;

    @Basic
    @Column(name = "Periodo", nullable = false)
    private int periodo;

    @Basic
    @Column(name = "ImporteTotal", nullable = false)
    private int importeTotal;

    @Basic
    @Column(name = "ImporteIVA", nullable = false)
    private int importeIva;

    @Basic
    @Column(name = "ImporteNeto", nullable = false)
    private int importeNeto;

    @Basic
    @Column(name = "Estado", nullable = false, length = 1)
    private String estado;

    @OneToMany(mappedBy = "facturaByNroFactura")
    private Collection<FacturaservicioEntity> facturaserviciosByNroFactura;

    public int getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(int nroFactura) {
        this.nroFactura = nroFactura;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(int importeTotal) {
        this.importeTotal = importeTotal;
    }

    public int getImporteIva() {
        return importeIva;
    }

    public void setImporteIva(int importeIva) {
        this.importeIva = importeIva;
    }

    public int getImporteNeto() {
        return importeNeto;
    }

    public void setImporteNeto(int importeNeto) {
        this.importeNeto = importeNeto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FacturaEntity that = (FacturaEntity) o;

        if (nroFactura != that.nroFactura) return false;
        if (periodo != that.periodo) return false;
        if (importeTotal != that.importeTotal) return false;
        if (importeIva != that.importeIva) return false;
        if (importeNeto != that.importeNeto) return false;
        if (estado != null ? !estado.equals(that.estado) : that.estado != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nroFactura;
        result = 31 * result + periodo;
        result = 31 * result + importeTotal;
        result = 31 * result + importeIva;
        result = 31 * result + importeNeto;
        result = 31 * result + (estado != null ? estado.hashCode() : 0);
        return result;
    }

    public Collection<FacturaservicioEntity> getFacturaserviciosByNroFactura() {
        return facturaserviciosByNroFactura;
    }

    public void setFacturaserviciosByNroFactura(Collection<FacturaservicioEntity> facturaserviciosByNroFactura) {
        this.facturaserviciosByNroFactura = facturaserviciosByNroFactura;
    }

    @Override
    public String toString() {
        return "FacturaEntity{" +
                "nroFactura=" + nroFactura +
                ", periodo=" + periodo +
                ", importeTotal=" + importeTotal +
                ", importeIva=" + importeIva +
                ", importeNeto=" + importeNeto +
                ", estado='" + estado + '\'' +
                '}';
    }
}
