package entidades;

import javax.persistence.*;

@Entity
public class FacturaservicioEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "NroServicio", nullable = false)
    private int nroServicio;

    private int nroFactura;

    @Basic
    @Column(name = "ImporteTotal", nullable = false)
    private int importeTotal;

    @Basic
    @Column(name = "ImporteIVA", nullable = false)
    private int importeIva;

    @Basic
    @Column(name = "ImporteNeto", nullable = false)
    private int importeNeto;

    @Basic
    @Column(name = "PorcentajeIVA", nullable = false, precision = 0)
    private double porcentajeIva;

    @ManyToOne
    @JoinColumn(name = "NroFactura", referencedColumnName = "NroFactura", nullable = false)
    private FacturaEntity facturaByNroFactura;

    public int getNroServicio() {
        return nroServicio;
    }

    public void setNroServicio(int nroServicio) {
        this.nroServicio = nroServicio;
    }

    public int getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(int nroFactura) {
        this.nroFactura = nroFactura;
    }

    public int getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(int importeTotal) {
        this.importeTotal = importeTotal;
    }

    public int getImporteIva() {
        return importeIva;
    }

    public void setImporteIva(int importeIva) {
        this.importeIva = importeIva;
    }

    public int getImporteNeto() {
        return importeNeto;
    }

    public void setImporteNeto(int importeNeto) {
        this.importeNeto = importeNeto;
    }

    public double getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(double porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FacturaservicioEntity that = (FacturaservicioEntity) o;

        if (nroServicio != that.nroServicio) return false;
        if (nroFactura != that.nroFactura) return false;
        if (importeTotal != that.importeTotal) return false;
        if (importeIva != that.importeIva) return false;
        if (importeNeto != that.importeNeto) return false;
        if (Double.compare(that.porcentajeIva, porcentajeIva) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = nroServicio;
        result = 31 * result + nroFactura;
        result = 31 * result + importeTotal;
        result = 31 * result + importeIva;
        result = 31 * result + importeNeto;
        temp = Double.doubleToLongBits(porcentajeIva);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public FacturaEntity getFacturaByNroFactura() {
        return facturaByNroFactura;
    }

    public void setFacturaByNroFactura(FacturaEntity facturaByNroFactura) {
        this.facturaByNroFactura = facturaByNroFactura;
    }
}
