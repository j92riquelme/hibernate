package operaciones;

import entidades.FacturaEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class App {

    public static void main(String[] args) {

        EntityManagerFactory entiManager = Persistence.createEntityManagerFactory("default");
        EntityManager entity = entiManager.createEntityManager();
        EntityTransaction transaction = entity.getTransaction();

        try {


            transaction.begin();

            //insert
            /*
            FacturaEntity factura = new FacturaEntity();
            factura.setNroFactura(129);
            factura.setEstado("A");
            factura.setImporteIva(50);
            factura.setImporteNeto(500);
            factura.setImporteTotal(550);
            factura.setPeriodo(202205);

            entity.persist(factura);
            */

            //select
            /*
            FacturaEntity factura = new FacturaEntity();
            factura = entity.find(FacturaEntity.class, 129);
            System.out.println(factura.toString());
            */

            //update
            /*
            FacturaEntity factura = new FacturaEntity();
            factura.setNroFactura(128);
            factura.setEstado("A");
            factura.setImporteIva(55);
            factura.setImporteNeto(550);
            factura.setImporteTotal(700);
            factura.setPeriodo(202206);
            entity.merge(factura);
            */

            //delete
            FacturaEntity factura = new FacturaEntity();
            factura = entity.find(FacturaEntity.class, 129);
            entity.remove(factura);

            transaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (transaction.isActive()){
                transaction.rollback();
            }
            entity.close();
            entiManager.close();
        }

    }

}
